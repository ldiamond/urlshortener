import os
from paste.deploy import loadapp
from cherrypy_server import cpwsgi_server
import getopt, sys

def getParams():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "c:p:", ["config=", "port="])
    except getopt.GetoptError, err:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    configFile = "production.ini"
    port = int(os.environ.get("PORT", 5000))
    for o, a in opts:
        if o in ("-c", "--config"):
            configFile = a
        elif o in ("-p", "--port"):
            port = a
        else:
            assert False, "unhandled option"

    return configFile, port



def main():

    configFile, port = getParams()

    wsgi_app = loadapp('config:' + configFile, relative_to='.')
    cpwsgi_server(wsgi_app, host='0.0.0.0', port=port,
                  numthreads=10, request_queue_size=200)

if __name__ == "__main__":
    main()



