from persistent.mapping import PersistentMapping
from persistent import Persistent
import random
import string

class Root(PersistentMapping):
    __parent__ = __name__ = None

class Shortener(PersistentMapping):
    """Shortens a URL. Chances of duplicate are 1/62^length. Worst case is it overrites an existing URL. No big deal.

    This is a key-value store
    """
    length = 10
    def shorten(self, url):
        """Takes a long URL in param and returns the short string representing this URL.
        Does not prevent chaining (shortening a shorturl). Loops would require predicting the generated short string, so unlikely.
        """
        ret = self.get(url)
        if ret is None:
            ret = self.__makeShortUrl(url)
        return ret.shortUrl

    def __makeShortUrl(self, url):
        """Creates a ShortUrl and corresponding LongUrl object and places them in the resource tree
        Takes a long URL in param.
        Returns the ShortUrl Object.
        """
        r = Shortener.randomString()
        longurl = LongUrl(url)
        longurl.__name__ = r
        longurl.__parent__ = self.__parent__

        shorturl = ShortUrl(r)
        shorturl.__name__ = url
        shorturl.__parent__ = self
        self[url] = shorturl
        self.__parent__[r] = longurl
        return shorturl

    @staticmethod
    def randomString():
        return ''.join(random.choice(string.letters + string.digits) for i in xrange(Shortener.length))


class ShortUrl(Persistent):
    def __init__(self, shortUrl):
        self.shortUrl = shortUrl

class LongUrl(Persistent):
    def __init__(self, longUrl):
        self.longUrl = longUrl

def appmaker(zodb_root):
    if not 'app_root' in zodb_root:
        app_root = Root()
        shortener = Shortener()
        shortener.__name__ = 'shortener'
        shortener.__parent__ = app_root
        app_root['shortener'] = shortener
        zodb_root['app_root'] = app_root
        import transaction
        transaction.commit()
    return zodb_root['app_root']
