import urlparse
import string


def getProperUrl(url):
    """Take a string and parse it as a URL. A URL is a very broad definition
    and includes many weird stuff including strings like 'aaaa' or '::::'. These
    just end up being a path. No reason we shouldn't accept them. However, we are not
    taking relative paths to our own domain, therefore we default the protocol to HTTP
    and if only a path is passed (i.e. no scheme specified), we treat it as the netloc.

    Also, let's not over restrict the inupt. That allows to shorten custom local urls.

    Argument is a string that represents the URL
    """
    url = url.strip()
    scheme, netloc, url, params, query, fragment = urlparse.urlparse(url)
    if scheme is '':
        scheme = 'http'
    if netloc is '':
        netloc = url
        url = ''
    u = urlparse.ParseResult(scheme, netloc, url, params, query, fragment)
    return u.geturl()


def getPath(url):
    scheme, netloc, path, params, query, fragment = urlparse.urlparse(url)
    return string.lstrip(path, '/')

def isSameHost(url, hosturl):
    sscheme, snetloc, surl, sparams, squery, sfragment = urlparse.urlparse(url)
    hscheme, hnetloc, hurl, hparams, hquery, hfragment = urlparse.urlparse(hosturl)
    return snetloc == hnetloc

