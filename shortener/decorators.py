def Layout(layout):
    def wrapper(wrapped):
        def newmethod(*args, **kwargs):
            if len(args) < 2:
                args = (None,) + args
            req = args[1]
            d = wrapped(*args,**kwargs)
            return dict(layout(req), **d)
        return newmethod
    return wrapper
