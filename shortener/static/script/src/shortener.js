!function(){
"use strict";
//This would usually be minimized and packed in ender.js, however, you may want to have a look ;)
    var reqwest = require('reqwest');
    var dr = require('domready');
    var builder = require('builder');

    builder.$ = require('ender');


    //Could have used a backbone view here. But that's simple too
    var ShortenedItem = function(shortUrl, longUrl, targetid){
        this.div = builder("li");
        var shortspan = builder("span");
        var longspan = builder("span");
        this.div.append(shortspan).append(builder('span', '  =>  ')).append(longspan);
        shortspan.text(shortUrl);
        longspan.text(longUrl);
        $(targetid).append(this.div);
    };


    //Simple object to hold some stuff we want to use outside.
    var shortener = {
        longurlId: '#longurl',
        shortenedListId: '#shortened > ul',
        shortenedDiv: '#shortened',
        buttonid: "#submitasync", 
        
        shorten: function(){
            var self = this;
            var success = function(result){
                if (result.status === 'success'){
                    //We might want to keep track of what we create and remove after X number, but unlikely to cause an issue
                    new ShortenedItem(result.shorturl, result.longurl, self.shortenedListId); 
                    //Make sure we display the div containing the results
                    $(self.shortenedDiv).show();
                }
            };
            var url = {'url':$(this.longurlId).val()}; 
                
            reqwest({
                url:"/shortener",
                method: 'post',
                type:'json',
                data:JSON.stringify(url),
                success:success
            });                

        },

        //Show the better form for people who have Javascript (didn't bind the enter event, would be nice to have)
        show: function(){
            $(this.buttonid).show();
            $(this.longurlId).show();
        }
    };
    

    provide('shortener', shortener);
    
    dr(function(){
        shortener.show();
    });

}();




