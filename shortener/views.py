from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from .decorators import Layout
from .models import Shortener
from .models import ShortUrl
from .models import LongUrl
from .models import Root
from .helpers import getProperUrl
from .helpers import isSameHost
from .helpers import getPath
from pyramid.url import static_url

#Should be provided in a package for this, along with most other strings used here.
success = 'success'
failed = 'failed'


def base(request):
    """This function returns all the necessary things for the base layout.
    To be used by the @Layout decorator
    """
    return {
        'css': static_url('shortener:static/css/bootstrap.css', request)
        ,'ender': static_url('shortener:static/script/ender.min.js', request)
        ,'shortener': static_url('shortener:static/script/src/shortener.js', request)
    }


@view_config(context=Root, request_method='GET', renderer='templates/home.jinja2')
@Layout(base)
def home(context, request):
    return {}



#This would be in a different module but I'm leaving it here so that it's easier to move around while reading.
#Ideally there would be two more functions: "shorten" and "expand" called from here.
def shortenOrExpand(shortener, url, hosturl):
    """Either shortens or expands depending on the given url
    """
    shorturl = None
    status = failed
    url = getProperUrl(url)
    hosturl = getProperUrl(hosturl)
    longurl = url
    if len(url) > 0:
        if isSameHost(url, hosturl):
            path = getPath(url)
            longurl = '?'
            shorturl = url
            if path in shortener.__parent__:
                longurl = shortener.__parent__[path].longUrl
        else:
            longurl = url
            shorturl = hosturl + '/' + shortener.shorten(longurl)
        status = success
    return {'status':status, 'shorturl':shorturl, 'longurl':longurl}


@view_config(context=Shortener, request_method='POST', xhr=True, renderer='json')
def createAsync(context, request):
    """Responding to async requests
    """
    return shortenOrExpand(context, request.json_body['url'], request.host_url)

@view_config(context=Shortener, request_method='POST', renderer='templates/home.jinja2')
@Layout(base)
def createSubmit(context, request):
    """Responding to POST for people without JS.
    """
    return shortenOrExpand(context, request.POST['url'], request.host_url)


@view_config(context=LongUrl, request_method='GET', renderer='templates/home.jinja2')
def expand(context, request):
    """Redirecting to the target URL
    """
    return HTTPFound(location=context.longUrl)


@view_config(renderer='templates/home.jinja2', context='pyramid.exceptions.NotFound')
@Layout(home)
def NotFound(context, request):
    """Handling 404
    We want to respond with 404 even if we serve the page, in case we care...
    """
    request.response.status = '404 Not Found'
    return {'error': """The link was not found. You can try another... but chances are you won't find any working url by trying randomly :("""}




