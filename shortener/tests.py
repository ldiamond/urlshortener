import unittest

from pyramid import testing

class ShortenerTests(unittest.TestCase):
    """A few tests, not exhaustive
    """
    def setUp(self):
        self.config = testing.setUp()
        from .models import Root
        from .models import Shortener
        self.root = Root()
        self.root['shortener'] = Shortener()
        self.shortener = self.root['shortener']
        self.shortener.__parent__ = self.root
        self.shortener.__name__ = 'shortener'

    def tearDown(self):
        testing.tearDown()

    def test_getProperUrl(self):
        from .helpers import getProperUrl

        self.assertEqual(getProperUrl(':::'), 'http://:::')
        self.assertEqual(getProperUrl('google.com'), 'http://google.com')
        self.assertEqual(getProperUrl('https://google.com'), 'https://google.com')
        self.assertEqual(getProperUrl('www.google.com'), 'http://www.google.com')
        self.assertEqual(getProperUrl(''),'http://')

    def test_shorten(self):
        from .helpers import getProperUrl
        shortener = self.shortener
        s1 = shortener.shorten('www.google.com')
        s2 = shortener.shorten('google.com')
        s3 = shortener.shorten('www.google.com')
        s4 = shortener.shorten('http://google.com')
        s5 = shortener.shorten(getProperUrl('google.com'))
        self.assertEqual(s1, s3)
        self.assertNotEqual(s2, s4)
        self.assertEqual(s4, s5)


    def test_shortenOrExpand(self):
        from .views import shortenOrExpand
        shortener = self.shortener
        root = self.root
        t1 = shortenOrExpand(shortener, 'www.google.com', 'lh')
        t2 = shortenOrExpand(shortener, 'google.com', 'http://lh')
        t3 = shortenOrExpand(shortener, 'http://google.com', 'lh')
        t4 = shortenOrExpand(shortener, t2.get('shorturl'), 'http://lh')
        t5 = shortenOrExpand(shortener, t2.get('shorturl'), 'otherhost')
        t6 = shortenOrExpand(shortener, t2.get('shorturl'), 'lh')

        self.assertEqual(t2, t3)
        self.assertNotEqual(t1, t2)
        self.assertEqual(t4.get('longurl'), 'http://google.com')
        self.assertNotEqual(t4, t5)
        self.assertEqual(t4, t6)
